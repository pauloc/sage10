# Instalación y configuración de Sage 10

https://bitbucket.org/pauloc/sage10/src/master/

## Instalar Sage

`composer create-project roots/sage eltema dev-master`

`cd eltema yarn && yarn start`


## Configurar la URL y el WP_ENV:

`eltema/webpack.mix.js -> .browserSync('sage.local');`

`/wp-config.php -> define('WP_ENV', 'development');`


## Instalar plugins (opcional):

`wp plugin install advanced-custom-fields --activate`

`wp plugin install fakerpress --activate`



### Gestion de campos advanced-custom-fields y directivas

`composer require log1x/sage-directives`

composer require "mwdelaney/sage-advanced-custom-fields"
composer require log1x/acf-composer


## Instalar paquetes y plugins:

`yarn add milligram-scss`

`yarn add swiper`


## Limpieza

Quitamos el Bootstrap.

`yarn remove bootstrap`

Y corregir errores bootstrap: app.js editor.scss, external.scss ...
Importar milligram en `external.scss`

## Contenidos

Creamos 5 páginas y 20 posts, más o menos.
Hacemos tres páginas hijas de otra.

Configuramos la home para que muestre una página.

## Plantillas Blade

https://laravel.com/docs/8.x/blade

Herencia (layouts) y pase de datos a plantillas hijas.


## Composers

https://laravel.com/docs/8.x/views#view-composers

Son metodos "callback" que se ejecutan cuando se visualiza una o varias vistas. Pasan datos a templates Blade específicos.

`wp acorn make:composer ExampleComposer`

Tienen una propiedad $views donde se definen las vistas a las que queremos que pasen datos.

Tienen dos metodos equivalentes 'with' y 'override' que son los que devuelven un array con los datos que queremos tener en las vistas. El 'with' es más 'blando' y puede ser pisado por las vistas. El 'override' pisa las variables que existan en las vistas.


## Componentes

https://laravel.com/docs/8.x/blade#components

Pasan datos a su clase como atributos, que pueden ser estáticos y dinámicos, los dinamicos llevan : por delante.

Comienzan por x-

`<x-componente color="red" :valor="$datos" ></x-componente>`

Los atributos requeridos se definen como parámetros en el constructor. Todas las propiedades públicas de la clase pasan a la vista del componente automáticamente.


## Referencias

Laravel Mix (configuración Webpackfrom):

https://laravel.com/docs/8.x/mix


Directivas Blade y más:

https://laravel.com/docs/8.x/blade


Directivas Blade específicas para WP y ACF:

https://github.com/log1x/sage-directives
https://log1x.github.io/sage-directives-docs/usage/wordpress.html

Otros repositorios:

https://github.com/roots
https://github.com/MWDelaney?tab=repositories


## Compatibilidad Woo

https://github.com/generoi/sage-woocommerce
composer require generoi/sage-woocommerce



