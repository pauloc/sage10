<?php

namespace App\View\Components;

use Roots\Acorn\View\Component;

class Carrusel extends Component
{
    /**
     * Has navigation.
     * @var bool
     */
    public $has_navigation;

    /**
     * Has pagination.
     * @var bool
     */
    public $has_pagination;

    /**
     * The number of posts to retrive.
     * @var string
     */
    public $post_num;

    /**
     * The posts query.
     * @var WP_Query
     */
    public $post_query;

    /**
     * Create the component instance.
     *
     * @param  string  $type
     * @param  int  $post_num
     * @return void
     */
    public function __construct($postsNum = 5, $hasNavigation = true, $hasPagination = true )
    {
        $this->post_num = $postsNum;
        $this->has_navigation = $hasNavigation;
        $this->has_pagination = $hasPagination;
        $this->post_query = $this->posts();
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return $this->view('components.carrusel');
    }


    private function posts () {
        $args = array(
          'post_type'      => 'post',
          'posts_per_page' => $this->post_num,
         );

        return new \WP_Query( $args );
    }

}
