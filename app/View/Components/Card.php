<?php

namespace App\View\Components;

use Roots\Acorn\View\Component;

class Card extends Component
{
    /**
     * The card type.
     * @var string
     */
    public $type;

    /**
     * The card title.
     * @var string
     */
    public $title;

    /**
     * The card url.
     * @var string
     */
    public $url;

    /**
     * The card excerpt.
     * @var string
     */
    public $excerpt;

    /**
     * The card date.
     * @var string
     */
    public $date;

    /**
     * Create the component instance.
     *
     * @param  string  $type
     * @param  WP_Object  $post
     * @return void
     */
    public function __construct($type = 'rounded', $post)
    {
        $this->type = $type;
        $this->title = $post->post_title;
        $this->url = esc_url( site_url( $post->post_name, null ) );
        $this->excerpt = $post->post_excerpt;
        $this->date = wp_date( 'd/m/Y' , strtotime($post->post_date) );
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return $this->view('components.card');
    }
}
