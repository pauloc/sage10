<?php

namespace App\View\Composers;

use Roots\Acorn\View\Composer;

class PaginasHijas extends Composer
{
    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        'front-page',
        'page-servicios',
    ];

    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with()
    {
        return [
            'children_pages' => $this->childs(),
            'clase' => 'page',
        ];
    }


  private function childs () {
    $args = array(
      'post_type'      => 'page',
      'posts_per_page' => -1,
      'post_parent'    => 2,
      'order'          => 'ASC',
      'orderby'        => 'menu_order'
     );

    $childs = new \WP_Query( $args );
    return $childs;
  }

}
