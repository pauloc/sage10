<?php

namespace App\View\Composers;

use Roots\Acorn\View\Composer;

class UltimosPosts extends Composer
{
    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        'front-page',
    ];

    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with()
    {
        return [
            'ultimos_posts' => $this->posts(),
            'clase' => 'article',
        ];
    }


  private function posts () {
    $args = array(
      'post_type'      => 'post',
      'posts_per_page' => 4,
      'order'          => 'DESC',
     );

    $posts = new \WP_Query( $args );
    return $posts;
  }

}
