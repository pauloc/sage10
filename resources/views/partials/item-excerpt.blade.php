<div class="column {{ $clase }}">
  <h2 class="entry-title">{{ $post->post_title }}</h2>
  <div class="entry-content">
    {!! $post->post_excerpt !!}
  </div>
</div>
