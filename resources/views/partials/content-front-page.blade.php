<div class="row pages">

@foreach ($children_pages->posts as $post)
  {{-- @include('partials.item-excerpt', ['clase' => 'page']) --}}
  <x-card type="squared" :post="$post" />
@endforeach

</div>


<div class="row posts">

  <x-carrusel :posts-num=12 :has-navigation=true :has-pagination=true />

</div>
