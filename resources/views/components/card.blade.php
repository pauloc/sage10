<div class="column card card--{{ $type }}">

  <div class="card__header">
    <h3 class="card__title">{{ $title }} </h3>
  </div>

  <div class="card__description">
    {!! \Illuminate\Support\Str::limit( $excerpt, 250, $end='...') !!}
  </div>


  <footer>
    <a href="{{ $url }}" class="button card__button">Leer más</a>
    <div class="card__fecha">
      <small>{{ $date }}</small>
    </div>
  </footer>
</div>
