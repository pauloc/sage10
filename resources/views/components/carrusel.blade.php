<div class="swiper-container">
  <div class="swiper-wrapper">
    @foreach ($post_query->posts as $post)
      <div class="swiper-slide">
        <x-card type="squared" :post="$post" />
      </div>
    @endforeach
  </div>

@if ($has_navigation)
  <!-- Add Arrows -->
  <div class="swiper-button-next"></div>
  <div class="swiper-button-prev"></div>
@endif

@if ($has_pagination)
  <!-- Add Pagination -->
  <div class="swiper-pagination"></div>
</div>
@endif
