/**
 * External Dependencies
 */
import 'jquery';
import Swiper, { Navigation, Pagination } from 'swiper';
// import 'bootstrap';

Swiper.use([Navigation, Pagination]);

$(document).ready(() => {
  // console.log('Hello world');

  if ( document.body.classList.contains('home') ) {

    var swiper = new Swiper('.swiper-container', {
      slidesPerView: 4,
      spaceBetween: 60,
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    });


  }

});
